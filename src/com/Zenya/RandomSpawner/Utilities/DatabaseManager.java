package com.Zenya.RandomSpawner.Utilities;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

public class DatabaseManager {
	
	Plugin plugin;;
	
	public DatabaseManager(Plugin plugin) {
		this.plugin = plugin;
		
		this.createTables();
	}
	
	// Utility to convert Location to world
	public String LocToWorld(Location loc) {
		String world = loc.getWorld().getName();
		
		return world;
	}
	
	// Utility to convert Location to [x, y, z]
	public ArrayList<Integer> LocToCoords(Location loc) {
		ArrayList<Integer> coords = new ArrayList<Integer>();
		coords.add(loc.getBlockX());
		coords.add(loc.getBlockY());
		coords.add(loc.getBlockZ());
		
		return coords;
	}
	
	public boolean getDatabaseExists() {
		File databaseFile = new File(plugin.getDataFolder(), "database.db");
		return databaseFile.exists();
	}
	
	public Connection connect() {
		String url = "jdbc:sqlite:" + plugin.getDataFolder() + File.separator + "database.db";
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(url);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public void createTables() {
		String sql = "CREATE TABLE IF NOT EXISTS blockdata ("
				+ "id integer PRIMARY KEY, "
				+ "world string NOT NULL, "
				+ "coordinates string NOT NULL);";
		
		try {
			Connection conn = this.connect();
			Statement statement = conn.createStatement();
			statement.execute(sql);
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void insertData(Location loc) {
		String world = LocToWorld(loc);
		String coords = LocToCoords(loc).toString();
		
		String sql = "INSERT INTO blockdata(world, coordinates) VALUES(?, ?)";
		
		try {
			Connection conn = this.connect();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, world);
			ps.setString(2, coords);
			ps.execute();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean queryDataExists(Location loc) {
		String world = LocToWorld(loc);
		String coords = LocToCoords(loc).toString();
		
		boolean hasNext = false;
		String sql = "SELECT * FROM blockdata WHERE world = ? AND coordinates = ?";
		
		try {
			Connection conn = this.connect();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, world);
			ps.setString(2, coords);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				hasNext = true;
			}
			
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return hasNext;
	}
	
	public void removeData(Location loc) {
		String world = LocToWorld(loc);
		String coords = LocToCoords(loc).toString();
		
		String sql = "DELETE FROM blockdata WHERE world = ? AND coordinates = ?";
		
		try {
			Connection conn = this.connect();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, world);
			ps.setString(2, coords);
			ps.execute();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
