package com.Zenya.RandomSpawner.Utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.Plugin;

public class ConfigManager {
	
	Plugin plugin;
	FileConfiguration config;
	
	public ConfigManager(Plugin plugin) {
		this.plugin = plugin;
		this.config = plugin.getConfig();
		
		if(!(this.getConfigExists())) {
			this.setDefaults();
		}
	}
	
	public void setDeveloperDefaults() { // For config revisions
		plugin.saveDefaultConfig();
		config.set("config-version", 1);
		
		for(EntityType type: EntityType.values()) {
			if(type.isAlive()) {
				String name = type.toString().toUpperCase();
				config.set("mobs."+name+".name", "&c"+name.toUpperCase()+" Spawner");
				config.set("mobs."+name+".drop-chance", 1);
			}
		}
		plugin.saveConfig();
	}
	
	public void setDefaults() {
		plugin.saveDefaultConfig();
	}
	
	public boolean getConfigExists() {
		File configFile = new File(plugin.getDataFolder(), "config.yml");
		return configFile.exists();
	}
	
	public String getName(EntityType et) {
		String entity = et.toString().toUpperCase();
		String name = config.getString("mobs."+entity+".name");
		name = ChatColor.translateAlternateColorCodes('&', name);
		
		return name;
	}
	
	public int getWeight(EntityType et) {
		String entity = et.toString().toUpperCase();
		int weight = config.getInt("mobs."+entity+".drop-chance");
		
		return weight;
	}
	
	public HashMap<EntityType, Integer> getWeightMap() {
		
		HashMap<EntityType, Integer> data = new HashMap<EntityType, Integer>();
		
		for(String entity : config.getConfigurationSection("mobs").getKeys(false)) {
			EntityType et = EntityType.valueOf(entity);
			
			int weight = config.getInt("mobs."+entity+".drop-chance");
			data.put(et, weight);
		}
		return data;
	}
	
	public ArrayList<EntityType> getEntities() {
		HashMap<EntityType, Integer> weightMap = getWeightMap();
		ArrayList<EntityType> entities = new ArrayList<EntityType>();
		
		entities.addAll(weightMap.keySet());
		return entities;
	}
	
	public int getTotalWeight() {
		
		HashMap<EntityType, Integer> weightMap = getWeightMap();
		int totalWeight = 0;
		
		for(EntityType et: weightMap.keySet()) {
			totalWeight += weightMap.get(et);
		}
		return totalWeight;
	}
	
	public FileConfiguration getInstance() {
		return config;
	}

}
