package com.Zenya.RandomSpawner.Utilities;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import com.Zenya.RandomSpawner.Main;

public class ItemUtils {
	
	Plugin plugin = Main.instance;
	ConfigManager cm = Main.configManager;
	
	// Get a random EntityType from config values
	public EntityType getRandomEntityType() {
		ArrayList<EntityType> entities = cm.getEntities();
		int totalWeight = cm.getTotalWeight();
		
		Random randObj = new Random();
		boolean done = false;
		
		while(!(done)) {
			EntityType et = entities.get(randObj.nextInt(entities.size()));
			int weight = cm.getWeight(et);
			int chance = randObj.nextInt(totalWeight);
			
			if(chance < weight) {
				done = true;
				return et;
			}
		}
		return EntityType.PIG;
	}
	
	// Check if player is holding a silk-touch pickaxe
	public boolean isHoldingSilkPick(Player player) {
		ItemStack item = player.getInventory().getItemInMainHand();
		
		if(!(item.getType().toString().toUpperCase().contains("PICKAXE"))) {
			return false;
		}
		
		if(!(item.containsEnchantment(Enchantment.SILK_TOUCH))) {
			return false;
		}
		
		return true;
	}
	
	// Get EntityType from a spawner ItemStack
	public EntityType getSpawnerItemEntityType(ItemStack item) {
		BlockStateMeta bsm = (BlockStateMeta) item.getItemMeta();
		CreatureSpawner cs = (CreatureSpawner) bsm.getBlockState();
		EntityType et = cs.getSpawnedType();
		
		return et;
	}
	
	// Get EntityType from a spawner Block
	public EntityType getSpawnerBlockEntityType(Block block) {
		CreatureSpawner cs = (CreatureSpawner) block.getState();
		EntityType et = cs.getSpawnedType();
		
		return et;
		}
	
	// Gives a spawner ItemStack
	public ItemStack setSpawnerItem(EntityType type) {
		// Getting data
		ItemStack is = new ItemStack(Material.SPAWNER);
		BlockStateMeta bsm = (BlockStateMeta) is.getItemMeta();
		CreatureSpawner cs = (CreatureSpawner) bsm.getBlockState();
		
		// Setting data
		cs.setSpawnedType(type);
		bsm.setBlockState(cs);
		is.setItemMeta(bsm);
		
		// Getting name
		String name = cm.getName(type);
		
		// Setting name
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		is.setItemMeta(meta);
		
		return is;
	}
}
