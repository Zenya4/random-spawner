package com.Zenya.RandomSpawner;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.Zenya.RandomSpawner.Listeners.EventListeners;
import com.Zenya.RandomSpawner.Utilities.ConfigManager;
import com.Zenya.RandomSpawner.Utilities.DatabaseManager;
import com.Zenya.RandomSpawner.Utilities.ItemUtils;

public class Main extends JavaPlugin {
	
	public static Main instance;
	public static ConfigManager configManager;
	public static DatabaseManager databaseManager;
	public static ItemUtils itemUtils;
	
	@Override
	public void onEnable() {
		instance = this;
		configManager = new ConfigManager(this);
		databaseManager = new DatabaseManager(this);
		itemUtils = new ItemUtils();
		
		
		Bukkit.getServer().getPluginManager().registerEvents(new EventListeners(), this);
	}
	
	@Override
	public void onDisable() {
		
	}

}
