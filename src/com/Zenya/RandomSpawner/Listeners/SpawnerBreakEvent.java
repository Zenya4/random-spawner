package com.Zenya.RandomSpawner.Listeners;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.Zenya.RandomSpawner.Main;
import com.Zenya.RandomSpawner.Utilities.DatabaseManager;
import com.Zenya.RandomSpawner.Utilities.ItemUtils;

public class SpawnerBreakEvent extends Event implements Cancellable {
	
	private final BlockBreakEvent event;
	Plugin plugin = Main.instance;
	ItemUtils itemUtils = new ItemUtils();
	DatabaseManager databaseManager = new DatabaseManager(plugin);
	
	public SpawnerBreakEvent(BlockBreakEvent event) {
		this.event = event;
		this.onSpawnerBreakEvent();
	}
	
	public void onSpawnerBreakEvent() {
		Block block = event.getBlock();
		Location loc = block.getLocation();
		
		ItemStack is = itemUtils.setSpawnerItem(itemUtils.getRandomEntityType());
		
		// Query from DB if it is a naturally-spawned block
		if(databaseManager.queryDataExists(loc)) {
			EntityType et = itemUtils.getSpawnerBlockEntityType(block);
			is = itemUtils.setSpawnerItem(et);
			
			// Remove dropped XP to prevent infinite XP
			event.setExpToDrop(0);
			
			databaseManager.removeData(loc);
		}
		
		event.setDropItems(false);
		event.getPlayer().getWorld().dropItemNaturally(loc, is);
	}
	
	private static final HandlerList Handler = new HandlerList();
	@SuppressWarnings("unused")
	private boolean isCancelled;

	@Override
	public HandlerList getHandlers() {
		return Handler;
	}
	
	public static HandlerList getHandlerList() {
		return Handler;
	}
	
	public boolean isCancelled() {
		return this.isCancelled();
	}
	
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}


}
