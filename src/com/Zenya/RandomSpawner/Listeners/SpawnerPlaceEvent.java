package com.Zenya.RandomSpawner.Listeners;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.plugin.Plugin;

import com.Zenya.RandomSpawner.Main;
import com.Zenya.RandomSpawner.Utilities.DatabaseManager;
import com.Zenya.RandomSpawner.Utilities.ItemUtils;

public class SpawnerPlaceEvent extends Event implements Cancellable {
	
	private final BlockPlaceEvent event;
	Plugin plugin = Main.instance;
	DatabaseManager databaseManager = Main.databaseManager;
	ItemUtils itemUtils = Main.itemUtils;
	
	public SpawnerPlaceEvent(BlockPlaceEvent event) {
		this.event = event;
		this.onSpawnerPlaceEvent();
	}
	
	public void onSpawnerPlaceEvent() {
		Block block = event.getBlock();
		Location loc = block.getLocation();
		
		EntityType et = itemUtils.getSpawnerItemEntityType(event.getItemInHand());
		event.getPlayer().sendMessage("Your hand item: " + et.toString());
		
		// Check to prevent errors, in case location is already in DB
		if(!(databaseManager.queryDataExists(loc))) {
			databaseManager.insertData(loc);
		}
		
		// Hacky method to bypass OP-only NBT spawner-placing
		BlockState bs = block.getState();
		CreatureSpawner cs = (CreatureSpawner) bs;
		cs.setSpawnedType(et);
		bs.update();
	}
	
	private static final HandlerList Handler = new HandlerList();
	@SuppressWarnings("unused")
	private boolean isCancelled;

	@Override
	public HandlerList getHandlers() {
		return Handler;
	}
	
	public static HandlerList getHandlerList() {
		return Handler;
	}
	
	public boolean isCancelled() {
		return this.isCancelled();
	}
	
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}


}
