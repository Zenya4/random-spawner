package com.Zenya.RandomSpawner.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import com.Zenya.RandomSpawner.Utilities.ItemUtils;

public class EventListeners implements Listener {
	
	ItemUtils itemUtils = new ItemUtils();
	
	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent event) {
		Block block = event.getBlock();
		Player player = event.getPlayer();

		// Not a spawner
		if(!(block.getType().equals(Material.SPAWNER))) {
			return;
		}
		
		// Player not holding silktouch pickaxe when mining
		if(!(itemUtils.isHoldingSilkPick(player))) {
			return;
		}
		
		SpawnerBreakEvent sbe = new SpawnerBreakEvent(event);
		Bukkit.getServer().getPluginManager().callEvent(sbe);
	}
	
	@EventHandler
	public void onBlockPlaceEvent(BlockPlaceEvent event) {
		Block block = event.getBlock();
		
		// Not a spawner
		if(!(block.getType().equals(Material.SPAWNER))) {
			return;
		}
		
		SpawnerPlaceEvent spe = new SpawnerPlaceEvent(event);
		Bukkit.getServer().getPluginManager().callEvent(spe);
	}

}
